<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Imports\UsersImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('duration', '>', 10)->paginate(10);
        return view('users.index', compact('users'));
    }

    public function viewUsers(){
        $users = User::select('user')->distinct()->get();

        return view('users.view', compact('users'));
    }
    public function viewUserClients($user){
        $userClients = User::where('user', $user)->where('duration', '>', 10)->orderByDesc('date')->limit(5)->get();
        $averageScore = User::where('user',$user)->where('duration', '>', 10)->avg('external_call_score');
        return view('users.userClients',compact('user','userClients','averageScore'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::select('user')->distinct()->get();
        $clients = User::select('client')->distinct()->get();
        $typeOfCalls= User::select('type_of_call')->distinct()->get();
        
        
        return view('users.create',compact('users','clients','typeOfCalls'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user' => 'required',
            'client' => 'required',
            'client_type' => 'required',
            'date' => 'required',
            'duration' => 'required|numeric',
            'type_of_call' => 'required',
            'external_call_score' =>'required|numeric|min:0|max:100',
        ]);

        User::create($request->all());

        return redirect()->route('users.index')
            ->with('success', 'Record added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user = $user;
        $users = User::select('user')->distinct()->get();
        $clients = User::select('client')->distinct()->get();
        $typeOfCalls= User::select('type_of_call')->distinct()->get();
        
        return view('users.edit', compact('user','users', 'clients', 'typeOfCalls'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'user' => 'required',
            'client' => 'required',
            'client_type' => 'required',
            'date' => 'required',
            'duration' => 'numeric',
            'external_call_score' => 'numeric|min:0|max:100',
        ]);

        $user->update($request->all());
        return redirect()->route('users.index')
            ->with('success', 'Log updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('users.index')
            ->with('success', 'Log deleted successfully');

    }
    public function importUser()
    {
        return view('import');
    }
    
    public function import(Request $request)
    {
        Excel::import(new UsersImport, $request->file('file'));
        return redirect()->route('users.index');
    }

    

}
