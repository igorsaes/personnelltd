<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'user' => $row['user'],
            'client' => $row['client'],
            'client_type' => $row['client_type'],
            'date' => $row['date'],
            'duration' => $row['duration'],
            'type_of_call' => $row['type_of_call'],
            'external_call_score' => $row['external_call_score'],
           
        ]);
    }
}
