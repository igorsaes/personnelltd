@extends('applayouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <div class="text-center mt-5">
            <a class="btn btn-primary" href="{{route('users.view')}}" title="Go back"><span>Go back</span> 
                    </a>
                </div>
        </div>
        <div class="col-md-12 text-center mt-5">
            <h1 class="mt-3">{{$user}}</h1>
            <h2 class="mt-2">Average User Score: {{$averageScore}}</h2>
            
            <table class="table table-bordered table-responsive-lg mt-5">
                <tr>

                    <th>Client</th>
                    <th>Client Type</th>
                    <th>Date</th>
                    <th>Duration</th>
                    <th>Type of call</th>
                    <th>External Call Score</th>
                </tr>
                @foreach ($userClients as $userClient)
                <tr>
                
                    <td>{{ $userClient->client }}</td>
                    <td>{{ $userClient->client_type }}</td>
                    <td>{{ $userClient->date }}</td>
                    <td>{{ $userClient->duration }}</td>
                    <td>{{ $userClient->type_of_call }}</td>
                    <td>{{ $userClient->external_call_score }}</td>
                    
                </tr>
                @endforeach
            </table>
           
           
        </div>
    </div>
</div>
@endsection
