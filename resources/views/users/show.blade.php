@extends('applayouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <div class="text-center mt-5">
            <a class="btn btn-primary" href="{{route('users.index')}}" title="Go back"><span>Go back</span> 
                    </a>
                </div>
        </div>
        <div class="col-md-12 mt-5">
            <h3 class="mt-3">User: {{$user->user}}</h3>
            <h3>Client: {{$user->client}}</h3>
            <h3>Client Type: {{$user->client_type}}</h3>
            <h3>Date: {{$user->date}}</h3>
            <h3>Call Duration: {{$user->duration}}</h3>
            <h3>Type of Call: {{$user->type_of_call}}</h3>
            <h3>External Call Score: {{$user->external_call_score}}</h3>
            
        </div>
    </div>
</div>
@endsection
