@extends('applayouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="text-center mt-5">
                    <p class="h1">Currently displaying all valid calls</p>
                </div>
                <div class="mt-3 mb-3 text-center">
                    <a class="btn btn-success" href="{{ route('users.create') }}" title="Add a new record"> <span>Add a new record</span> 
                          
                    </a>
                </div>
    
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
                <table class="table table-bordered table-responsive-lg mt-5">
                    <tr>
                        <th>User</th>
                        <th>Client</th>
                        <th>Client Type</th>
                        <th>Date</th>
                        <th>Duration</th>
                        <th>Type of Call</th>
                        <th>External call score</th>
                        <th>Action</th>
                    </tr>
                    @foreach ($users as $user)
                    
                    <tr>
                        <td>{{ $user->user }}</td>
                        <td>{{ $user->client }}</td>
                        <td>{{ $user->client_type }}</td>
                        <td>{{ $user->date }}</td>
                        <td>{{ $user->duration }}</td>
                        <td>{{ $user->type_of_call }}</td>
                        <td>{{ $user->external_call_score }}</td>
                        <td>
                            <form action="{{ route('users.destroy', $user->id) }}" method="POST">
        
                                <a href="{{ route('users.show', $user->id) }}" title="show">
                                    <i class="fas fa-eye text-success  fa-lg"></i>
                                </a>
        
                                <a href="{{ route('users.edit', $user->id) }}">
                                    <i class="fas fa-edit  fa-lg"></i>
        
                                </a>
        
                                @csrf
                                @method('DELETE')
        
                                <button type="submit" title="delete" style="border: none; background-color:transparent;">
                                    <i class="fas fa-trash fa-lg text-danger"></i>
        
                                </button>
                            </form>
                        </td>
                    </tr>
                    </tr>
                   
                    @endforeach
                </table>
                <div class="d-flex justify-content-center">
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection