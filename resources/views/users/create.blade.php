@extends('applayouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="pull-left">
                <h2>Add New Record</h2>
            </div>
            <div class="text-center">
                <a class="btn btn-primary" href="{{route('users.index')}}" title="Go back"><span>Go back</span> 
                    </a>
            </div>
        </div>
    </div>

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="user"><strong>User:</strong></label>
                    <select class="form-control" id="user" name="user">
                        @foreach ($users as $user)
                        <option>{{$user->user}}</option>
                        @endforeach

                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="client"><strong>Client:</strong></label>
                    <select class="form-control" id="client" name="client">
                        @foreach ($clients as $client)
                        <option>{{$client->client}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="client_type"><strong>Client Type</strong></label>
                    <input type="text" name="client_type" class="form-control" id="client_type">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="date"><strong>Date:</strong></label>
                    <input type="date" name="date" class="form-control" id="date">

                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="duration"><strong>Duration:</strong></label>
                    <input type="number" name="duration" class="form-control" id="duration">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="type_of_call"><strong>Type Of Call</strong></label>
                    <select class="form-control" id="type_of_call" name="type_of_call">
                        @foreach ($typeOfCalls as $typeOfCall)
                        <option>{{$typeOfCall->type_of_call}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="external_call_score"><strong>External Call Score:</strong></label>
                    <input type="number" name="external_call_score" class="form-control" id="external_call_score">
                </div>
            </div>
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary mt-3">Submit</button>
            </div>
        </div>

    </form>
</div>
@endsection