@extends('applayouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="text-center mt-5">
                    <p class="h1">Currently displaying all Users</p>
                </div>
                <table class="table table-bordered table-responsive-lg mt-5">
                    <tr>
    
                        <th>User</th>
                        <th>View Clients</th>
                    </tr>
                    @foreach ($users as $user)
                    <tr>
                    
                        <td>{{ $user->user }}</td>
                        <td>
                        <a href="{{route('users.userClients', $user->user)}}" title="show">
                            <i class="fas fa-eye text-success  fa-lg"></i>
                        </a>
                    </td>
                    </tr>
                    @endforeach
                </table>

            </div>
        </div>
    </div>
@endsection