<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// IMPORT ROUTES----------------
Route::get("/", [UserController::class, "importUser"]);
Route::post("import", [UserController::class, "import"])->name('import');
//-------------------------

//USER-CLIENT ROUTES----------------
Route::get("users/", [UserController::class, "index"])->name('users.index');
Route::get("users/view", [UserController::class, "viewUsers"])->name('users.view');
Route::get("users/view/{user}", [UserController::class, "viewUserClients"])->name('users.userClients');
//-----------------------------------

//CRUD---------------------
Route::get('users/create', [UserController::class, 'create'])->name('users.create');
Route::post('users/', [UserController::class, 'store'])->name('users.store');
Route::get('users/{user}', [UserController::class, 'show'])->name('users.show');
Route::get('users/{user}/edit', [UserController::class, 'edit'])->name('users.edit');
Route::put('users/{user}', [UserController::class, 'update'])->name('users.update');
Route::delete('users/{user}', [UserController::class, 'destroy'])->name('users.destroy');
//------------------------





